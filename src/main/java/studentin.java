import java.util.ArrayList;

/**
 * Created by Marek on 2017-01-22.
 */
public interface studentin {
    ArrayList<Double> GetMarks();
    Double GetAvarege();
    void AddMark(Double mark);
    public Double calculateAvarage();
}