/**
 * Created by Marek on 2017-01-22.
 */
public class person {
    private  adress Adres;

    private String FirstName;
    private String SurName;
    private int DayofBirth;
    private int MonthofBirth;
    private int YearofBirth;

    public person(){
        Adres = new adress(null, null, null, 0,0, 0);
        DayofBirth = 0;
        MonthofBirth = 0;
        YearofBirth = 0;
        FirstName = null;
        SurName = null;
    }
    public person(String street, String city, String country, int zipcode, int numberbuilding, int numberhouse, String name, String surname, int dayofbirth, int monthofbirty, int yearofbirth){
        Adres = new adress(street, city, country, zipcode,numberbuilding, numberhouse);
        FirstName = name;
        SurName = surname;
        DayofBirth = dayofbirth;
        MonthofBirth = monthofbirty;
        YearofBirth = yearofbirth;
    }

    public String getFirstName() { return FirstName; }
    public String getSurnName() { return SurName; }
    public int getDayofBirth() { return DayofBirth; }
    public int getMonthofBirth() { return MonthofBirth; }
    public int getYearofBirth() { return YearofBirth; }

    public void setFirstName(String name ){ FirstName = name; }
    public void setSurName(String surname ){ SurName = surname; }
    public void setDayofBirth(int day ){ DayofBirth = day; }
    public void setMonthofBirth(int moth){ MonthofBirth = moth; }
    public void setYearofBirth(int year){ YearofBirth = year; }


    public void print(){
        System.out.print("Person\n Name:" + FirstName + "\nSurname: " + SurName + "\n Born:" + DayofBirth + "-" + MonthofBirth + "-" + YearofBirth + "\n");
    }

}
