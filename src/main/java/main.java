/**
 * Created by Marek on 2017-01-22.
 */

import java.util.concurrent.*;
import java.util.Random;
import java.text.DecimalFormat;

public class main {
    static Random rand = new Random();
    private static Double randomMark() {  return 2.0 + 3.0 * rand.nextDouble(); }

    public static void main(String[] args) {
        final int NumberofStudents = 10;
        int NumberofMarks = 5;
        DecimalFormat twoDForm = new DecimalFormat("#.##");

        ConcurrentHashMap<Integer, student> Studenci = new ConcurrentHashMap<>(NumberofStudents);

        // Add students
        for(int i = 1; i<NumberofStudents+1; i++) {
            Studenci.put(i, new student());
        }

        // Add random marks
        for(int i=1; i<NumberofStudents+1; i++) {
            for (int j = 0; j < NumberofMarks; j++) {
                Studenci.get(i).AddMark(randomMark());
            }
        }

        for(int i=1; i<NumberofStudents + 1; i++) {

            try {
                Studenci.get(i).calculateAvarage();
            }catch(ArrayIndexOutOfBoundsException e) {
                System.out.println("Exception thrown  :" + e);
            }

            System.out.print("The index number of student: " + i + "\nMarks: ");
            for (int j = 0; j < NumberofMarks; j++) {
                System.out.print("" + twoDForm.format(Studenci.get(i).GetMarks().get(j)) + " | ");
            }

            System.out.print("\nAvarege: "+ twoDForm.format(Studenci.get(i).GetAvarege())  + "\n\n");
        }

    }
}
