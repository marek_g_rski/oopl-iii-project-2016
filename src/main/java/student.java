/**
 * Created by Marek on 2017-01-22.
 */

import java.util.ArrayList;

public class student extends person implements studentin {
    private int indexnumber;
    private Double Avarege;
    private ArrayList<Double> Marks;

    public student(){
        super();
        Marks = new ArrayList<Double>();
    }



    public ArrayList<Double> GetMarks(){ return Marks; }
    public Double GetAvarege() { return Avarege; }
    public void AddMark(Double mark){ Marks.add(mark); }

    public void print(){
        System.out.print("Student\n Name:" + getFirstName() + "\nSurname: " + getSurnName() + "\n Born:" + getDayofBirth() + "-" + getMonthofBirth() + "-" + getYearofBirth() + "\nIndex number" + indexnumber + "\n");
    }

    public Double calculateAvarage(){
        double sum = 0;

        for(int i = 0; i < Marks.size(); i++){
            sum += Marks.get(i);
        }

        Avarege = sum / Marks.size();
        return  this.Avarege;
    }

}
