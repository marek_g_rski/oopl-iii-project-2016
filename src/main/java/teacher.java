/**
 * Created by Marek on 2017-01-22.
 */
public class teacher extends person implements teachin {

    private int teacherID;
    private String speciality;

    public teacher(){
        teacherID = 0;
        speciality = null;
    }

    public teacher(int id, String spec){
        teacherID = id;
        speciality = spec;
    }

    public int getTeacherID() { return teacherID; }
    public String getSpeciality() { return speciality; }

    public void setTeacherID(int id) { teacherID = id; }
    public void setSpeciality (String spec ) { speciality = spec; }

    public void print(){
        System.out.print("Student\n Name:" + getFirstName() + "\nSurname: " + getSurnName() + "\n Born:" + getDayofBirth() + "-" + getMonthofBirth() + "-" + getYearofBirth() + "\nTeacher number" + teacherID + "\n " + "\nSpeciality" + speciality + "\n " );
    }
}
